class Trash {
  constructor() {
    this.speed = floor(random(3, 6));  // hastigheden på skraldet (floor: tallet kommer som en integer, dvs. et afrundet til)
    this.posX = width + 60;
    this.posY = random(0, height);
    this.size =  {w: 99, h: 102};
    /*
    this.size = floor(random(15, 35));
    this.posX = 0;
    this.posY = height / 2;
    
    this.width = 86;
    this.height = 89;
    this.trash = "assets/trash.png";
    */
  }

  show() { // Skal være et billede
    image(trashPic,
      this.posX,
      this.posY,
      this.size.w,
      this.size.h
      );
  }

  move() {  //moving behaviors
    this.posX -= this.speed;  // skraldets bevægelse mod 0 på y-aksen
  }
}