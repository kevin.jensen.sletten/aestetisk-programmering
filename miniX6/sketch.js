let trashPic, playerPic, lifePic;
let player;

let lives = 3; // bestemmer hvor mange liv spilleren starter med 
let trash = [];
let min_trash = 4; // hvor mange skraldeøer der er i spillet
let dodged = 0; // antallet af undviget skralde øer
let level = 1

/* must have:
- spiller (SKAL være billede af båd)
- objekter der skal undviges (SKAL være billede af trash)
*/

/* should have:
- restart-funktion til at genstarte spiller efter game-over
- billede ødelægger lidt collisionCheck ift. hvor hit registreres?
*/ 

/* nice to have:
- objekter der giver spilleren ekstra liv (SKAL være billede af manden)
- score i form af hvor lang tid man har holdt sig i live (evt. MM : SS)
- intro tekst?
- (random bølger på kanvasset for at styrke idéen om at det er et hav)
- hastighederne på skralde øer stiger i højere levels
- forskellige art for hvert level (vandet bliver mere og mere olieret, f.eks.?)
- i stedet "lives left" skal der være ikoner (ifm. hjerter)
*/


function preload() { 
    trashPic = loadImage('trash.png');
    playerPic = loadImage('boat.png');
}

function setup(){
  createCanvas(windowWidth, 800); // når man bevæger sig ned af y-asken flytter skærmen sig
  background("#2B65EC"); // havblå-baggrund
  player = new Player();

  for (let i = 0; i < min_trash; i++) {
    trash.push(new Trash());
  }
}

function draw() {
    background("#2B65EC");
    player.show();
    checkHit();
    showTrash();
    checkTrashNum();
    checkLivesLeft();
    checkDodged();
    levelUp();
    textAlign(CENTER);
    fill(255);
    textSize(30);
    text("You're on level " + level, width / 2, 50);
    textAlign(CENTER);
    fill(255);
    textSize(20);
    text("You have " + lives + " lives left!", width / 2, 80);



    if (keyIsDown(UP_ARROW)) {
        player.moveUp();
    } else if (keyIsDown(DOWN_ARROW)) {
        player.moveDown();
    } // frem- og tilbage-funktionalitet
    /* else if  (keyIsDown(LEFT_ARROW)) {
        player.moveLeft();
    } else if (keyIsDown(RIGHT_ARROW)) {
        player.moveRight();
      
    } */
}

function checkTrashNum() {
    if (trash.length < min_trash) { // hvis der er mindre end det mindste antal af biler tilføjes der flere
        trash.push(new Trash()); //skaber et nyt trash-instance (trash.js)
    }
    /* lige nu, sker der det at ellipserne (pseudo-skraldet) forsætter i uendelighed ud i den negative x-akse (dvs. < 0)
    og derfor er min_trash altid uændret da de aldrig forsvinder, der skal derfor tjekkes efter om ellipserne har ramt posX = -10 
    og tilføjes til dodged; men hvordan? */
}

function showTrash() {
    // viser skralde øer og flytter skraldet på tværs af skærmen (y -= ) - se move() i trash.js 
    for (let i = 0; i < trash.length; i++) { // trash.length definerer antallet af skralde øer
        trash[i].show();
        trash[i].move();
    }
}

function checkHit() { //tjekker om spilleren bliver ramt af et skralde-objekt
    // conditional statement der tjekker om spiller har flere liv
        // hvis checkHit() "trigger" skal der trækkes et liv fra lives
    // ellers skal de noLoop()'s og GAME OVER
    //hvis spilleren bliver ramt trækkes -1 fra lives-arrayen og fjerner skraldet spilleren bliver ramt af
    let distance;
    for (let i = 0; i < trash.length; i++) {
        distance = dist( // udregner afstanden mellem skibet og skraldet
            player.posX,
            player.posY,
            trash[i].posX,
            trash[i].posY
            );
  
        if (distance < player.size.w / 2) {
            trash.splice(i, 1);
            lives--;
            trash.push(new Trash());
            console.log("You got hit! You have " + lives + " lives left.");
        } 
    }
}

function checkLivesLeft() {
    if (lives === 0) { // når spilleren har mistet sit sidste liv --> game over!
        noLoop();
        console.log("That was your last life. Game over...")
        textAlign(CENTER);
        fill(255);
        textSize(25);
        text("There's too much trash in the ocean...", width / 2, height / 2 + 15);
        text("Better luck on the next planet!", width / 2, height / 2 - 15);
        
    }
}

function checkDodged() {
    for (let i = 0; i < trash.length; i++) {
        if (trash[i].posX < -100) {
        dodged++;
        trash.splice(i,1);
        console.log("Dodged trash-islands: " + dodged);
        // hvis spilleren undviger skraldet fjernes dét objekt fra arrayen (og skal tilføjes til scoren, som er dodged)
        }
    }
}


function levelUp() { // når skralde øerne bliver undveget --> niveauet stiger...
    if (dodged === 4 ) {
        min_trash = 6;
        level = 2;
    } else if (dodged === 10) {
        min_trash = 7;
        level = 3;
    } else if (dodged === 17) {
        min_trash = 8;
        level = 4;
    } else if (dodged === 25) {
        min_trash = 9;
        level = 5;
    } else if (dodged === 34) {
        min_trash = 10;
        level = 6;
    } else if (dodged === 44) {
        min_trash = 11;
        level = 7;
    } else if (dodged === 55) {
        min_trash = 12;
        level = 8;
    } else if (dodged === 67) {
        min_trash = 13;
        level = 9;
    } else if (dodged === 80) {
        min_trash = 14;
        level = 10;
    } else if (dodged === 94) {
        min_trash = 15;
        level = 11;
    } else if (dodged === 109) {
        min_trash = 16;
        level = 12;
    } else if (dodged === 125) {
        min_trash = 17;
        level = 13;
    } else if (dodged === 142) {
        min_trash = 18;
        level = 15;
    } else if (dodged === 160) {
        min_trash = 19;
        level = 16;
    } else if (dodged === 179) {
        min_trash = 20;
        level = 17;
    } else if (dodged === 199) {
        min_trash = 21;
        level = 18;
    } else if (dodged === 220) {
        min_trash = 22;
        level = 19;
    } else if (dodged === 245) {
        min_trash = 30;
        level = 20;
    } else if (dodged === 290) {
        min_trash = 35;
        level = 21;
    } else if (dodged === 340) {
        min_trash = 45;
        level = 22;
    } else if (dodged === 400) {
        min_trash = 60;
        level = "EARTH SAVER!!";
    }
}
 
 