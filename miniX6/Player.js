class Player { // Skal være et billede + det med bevægelsen
  constructor() {
    this.posX = 100; // spiller starter i venstre side af skærmen
    this.posY = height / 2;
    //this.width = 86;
    //this.height = 89;
    //this.player = "assets/boat.png";
    this.moveFactor = 5; // bevægelseshastighed for skibet (spilleren)
    this.size = {w: 158, h: 115};
  }

  show() {
    image(playerPic,
      this.posX,
      this.posY,
      this.size.w,
      this.size.h
      );
  }

  moveUp() {
    this.posY -= this.moveFactor;
    // checker om vi er for højt oppe
    if (this.posY < 15) {
      this.posY = 15;
    }
  }

  moveDown() {
    this.posY += this.moveFactor;
    // checker om vi er for langt nede
    if (this.posY > height - 15) {
      this.posY = height - 15;
    }
  }

  moveLeft() {
    this.posX -= this.moveFactor;
    // checker om vi er for langt til venstre
    if (this.posX < 15) {
      this.posX = 15;
    }
  }
  
  moveRight() {
    this.posX += this.moveFactor;
    // checker om vi er for langt til højre
    if (this.posX > width - 15) {
      this.posX = width - 15;
    }
  }
}