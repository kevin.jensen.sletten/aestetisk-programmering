# MiniX6: Games with Objects

#### Trash Trip

![]() <img src="MiniX6.png" width="600" height="400">

Her er min [miniX6](https://bustersiem.gitlab.io/aestetisk-programmering/miniX6/index.html) og det tilhørende link til mit spils [source code](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX6/sketch.js), og filerne til [spilleren](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX6/Player.js) og [skraldet](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX6/Trash.js).


### RunMe

Mit spil, ***Trash Trip***, handler om at man som spiller 'kommanderer' et skib, der skal navigerer havet, undvige skrald og samle personer, der er faldet over bord op. Spillet fungerer således at man har fem liv, og når man sejler ind i en 'skraldeø' mister man et liv. For at få liv tilbage samler man de her mennesker, som er i vandet af og til, op - målet. med spillet er at undvige så meget skrald som muligt og se hvor længe man overlever på dette forurenede stykke vand! Når man undviger skrald stiger sværhedsniveauet ved spillet løbende, det vil sige, at jo mere skrald man har undviget desto hurtigere kommer det nye skrald, og hov, der kommer forresten også mere skrald at undvige i takt med at man stiger i niveau. 

Kort sagt består mit spil af to  komponenter: Spilleren i form af en båd og 'skraldeøerne' som skal undviges. De to 'karakter' er hver især lavet som individuelle classes og har hver sin egen .js-fil (som jeg har linket til hver for sig i toppen af denne ReadMe). Min `Player`-class fungerer lidt ligesom Pacman i Francis Lams spil *Tofu Go!* fra 2018, som bliver gennemgået i Aesthetic Programming-grundbogen, hvor spilleren bevæger sig op og ned af y-aksen, men hvor der dog kommer objekter som skal undviges (skraldet) - i stedet for at de skal fanges i form af de tofu-stykker som han har tiltænkt i sit spil. Min `Trash`-class er objektet som spilleren skal undvige og, som jeg nævnte tidligere, som spillet fortsættter bliver sværhedsniveauet sværere og sværere.

Først og fremmest lavede jeg en tom array `trash = []`, som er en container for de *instances*, der bliver lavet af skraldeøerne igennem spillet. For at skabe flere af min `Trash`-class på samme tid lavede jeg en funktion `showTrash()`, hvor jeg brugte en for-loop til at loope funktioner fra selve classen:
```
for (let i = 0; i < trash.length; i++) { 
    trash[i].show();
    trash[i].move();
}
```
Funktionerne `.show()` og `.move()` er funktionaliteter, som jeg har lavet i selve min [class-fil](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX6/minix5sketch.js), de gør at objektet bliver vist på kanvasset og at classen har en bevægelsefunktionalitet mod x-aksen 0-punkt. Hvis jeg ville have et klassik arkade-agtigt spil, hvor essensen var, at man som spiller skulle undvige diverse objekter, skulle min kode også have en funktionalitet, der tjekker om spilleren har en kollision med et givent objekt. Det løste jeg ved at lave funktionen `checkHit()`, som ser ud således:
```
function checkHit() {
    let distance;
    for (let i = 0; i < trash.length; i++) {
        distance = dist(
            player.posX, player.posY,
            trash[i].posX, trash[i].posY
            );
  
        if (distance < player.size) {
            trash.splice(i, 1);
            lives--;
            trash.push(new Trash());
            console.log(
                "You got hit! You have " + lives + " lives left.
            ");
        } 
    }
}
```
I funktionen ser man et for-loop, hvori der er et if-statement som tjekker om variablen `distance` er *mindre* end spillerens størrelse (kort sagt: om spilleren rører objektet). For at programmet ved hvilken afstand, der er på tale har jeg i selve `distance`-variablen defineret at det skal være afstanden mellem spillerens y- og x-position og et vilkårligt objektet fra `trash`-arrayens y- og x-position. Igen er værdier fra objekternes respektive classes på spil i form af `.posX` og `.posY` som er lokale til begge objekter (altså `Player` og `Trash`). HVIS spilleren rammes af et objekt, det vil sige, hvis if-statementet er *true*, bliver der fjernet et objekt fra `trash`-arrayen, og trukket et liv fra en variabel `lives` (der bestemmer hvornår det er game-over, men for at holde denne ReadMe i en nogenlunde længde er dét det eneste jeg skriver om denne for nu) - derudover bliver der også logget til consolen, så man kan se at koden har eksekveret.

Hvis jeg ville inkorporerer funktionaliteten med stigende sværhedsgrader i mit skulle jeg også have en funktion, der tjekker for om spilleren har undveget skraldet; med andre ord skulle der tjekkes om skraldet rører venstre ende af kanvasset. Funktionen, `checkDodged()`, som jeg kom frem til ser sådan ud:
```
function checkDodged() {
    for (let i = 0; i < trash.length; i++) {
        if (trash[i].posX < -50) {
        dodged++;
        trash.splice(i,1);
        console.log("Dodged trash-islands: " + dodged);
        }
    }
}
```
Det er umiddelbart en simpel funktion: Der bliver tjekket i et if-statement om et vilkårligt element i min `trash`-array er på en `.posX` (altså x-akse position), der er *mindre* end -50 - det ser lidt mere *æstestisk* ud, at skraldet ikke forsvinder så snart de rører venstre side af kanvasset, derfor er det konditionen for at koden eksekverer sat til -50, som er lidt udenfor kanvasset (og udenfor spillerens øje). Når koden så løber bliver der tilføjet én til en variabel som jeg har navngivet `dodged`; denne variabel styrer hvilket "level" spilleren er på i et if-else-statement som jeg har lavet sidst i min `sketch`-fil. `levelUp()`-funktionen styrer altså hvor mange skraldeøer der mindst skal vises på kanvasset, og dette tal stiger når man når et nyt level - som stiger i takt med at variablen `dodged` får en højere værdi:
```
function levelUp() {
    if (dodged === 4 ) {
        min_trash = 6
        level = 2;
    } else if (dodged === 10) {
        min_trash = 7
        level = 3;
    } else if (dodged === 17) {
        min_trash = 8
        level = 4;
    } else if (dodged === 25) {
        min_trash = 9
        level = 5;
    }    [...]
}
```

### ReadMe

Mit spil er inspireret af en en af de meget prominente problemstilling vi ser i vores samfund i 2023 (og som egentlig har været et pressende problem i mange år): Miljøforurening. Specielt har jeg taget inspiration fra de affaldssøer der er i den nordlige del af Stillehavet ud for henholdsvis Japan og USA's kyster, som er blevet navngivet "the *Great Pacific Garbage Patch*". Problemet spiller især en stor rolle grundet at det affald der foråsager de her søer af affald, blandt andet indeholder store mængder af plast og mikroplast. Disse søer foråsager blandet andet problemer som sammenfiltring og *ghost fishing*, det vil sige at fiskene i vandet bliver fanget og dræbt af stumper og rester fra skraldet. Specielt er tabte fiskenet farlige, fordi de bliver ved med at 'fiske', selvom de er blevet tabt - deraf '*ghost*' da de ikke længere er under kontrol af fiskeren, men stadig forvolder dyrelivet stor skade. Men der opstår også andre problemer for dyrene i havene for eksempel i form af indtagelsesproblemer, da fisk og andet liv måske spiser plastik eller andre rester fra skraldet - som endeligt kan ende med at stoppe dyrene fra at spise rigtig mad (og på samme tid kan det være at vi mennesker ender med disse fisk på vores tallerkener).

I forhold til *abstraction* og karakteristikkerne af *object-oriented programming* (herefter OOP) kan man sige at mit spil i stor stil har haft indflydelse af, at OOP er en måde hvorpå man kan reflektere over hvordan verden ser ud og, som programmør (af spillet), sætte sit eget perspektiv på det syn, Soon and Cox (2020) beskriver det som følgende:

> OOP is designed to reflect the way the world is organized and imagined, at least **from the computer programmers’ perspective**. It provides an understanding of the ways in which relatively independent objects operate through their relation to other objects. (s. 160)

Derudover, argumenterer de yderligere for at objekter vi træffer i hverdagslivet bliver "komprimeret og abstrakteret" i hvad de - i Fuller og Goffeys ord - kalder en "*object-oriented* modeling of the world": 

> (...) this object-oriented modeling of the world is a social-technical practice, “compressing and abstracting relations operative at different scales of reality, composing new forms of agency.” In their opinion, this agency operates across the computational and material arrangement of everyday encounters. (s. 161)

Og man må vel sige, at der i stor stil er noget politisk vægtet over at lave et spil, der (**muligvis**) skaber noget fokus på problemer, som vi i høj grad har mulighed for at støde på i vores hverdag. På samme tid er de her politiske problemstillinger, som man i nogle tilfælde kan støde på, om det er i spil, litteratur (som for eksempel Aesthetic Programming-grundbogen, i form af de feministiske træk den har) eller hvor det end er, er meget tæt på os da det også typisk er problemer vedører os som mennesker og omhandler hverdagslivet. For mig, som programmøren og udvikler af *Trash Trip*, er den grønne omstilling og klimaforuering noget som jeg går relativt meget op i; jeg er på ingen måde en 'fanatiker' eller forkæmper for det, men det er bestemt noget jeg tænker på fra tid til anden. Derfor ville jeg skabe et spil hvori jeg prøver at lave en agenda for netop at skabe lidt opmærksomhed på dette problem - som lidt er i stil med det jeg fortolker fra kapitlet om OOP i bogen.

***

#### Referenceliste

Great Pacific Garbage Patch. National Geographic. Retrieved March 21, 2023, from https://education.nationalgeographic.org/resource/great-pacific-garbage-patch/.

Parker, D. & Office of Response & Restoration. (Revised 2023, March 21). Garbage Patches. OR&R’s Marine Debris Program. Retrieved March 21, 2023, from https://marinedebris.noaa.gov/info/patch.html.

Soon, W., & Cox, G. (2020). Aesthetic Programming: A Handbook of Software Studies  (pp. 147-158). https://aesthetic-programming.net.
