# MiniX5: A Generative Program

#### THE *PHONIC* ALPHABET

<img src="MiniX5.png" width=windowWidth height=windowHeight>

Her er min [miniX5](https://bustersiem.gitlab.io/aestetisk-programmering/miniX5/index.html) og det tilhørende link til min [source code](https://gitlab.com/bustersiem/aestetisk-programmering/-/blob/main/miniX5/minix5sketch.js).


### RunMe

I denne miniX har jeg brugt arrays, som jeg har kaldt `phonicVowels` og `phonicConsonants` til at holde på de foniske ord fra NATO's fonetiske alfabet. For at få et tilfældigt ord lavede jeg en `random` til generator, for at vælge et tal ud fra 1 til 104; grunden til at det var til 104, er at der er 26 ord i det fonetiske alfabet (i hvertfald i det internationale af dem) og det har jeg ganget med fire, hvilket i skrivende faktisk er noget overflødigt nu hvor jeg tænker over det - det burde bare have været 1 til 26, men nok om det. Min `random`-generator funktion ser således ud:
```
function randomIntFromInterval(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}
```
Og som man kan se i koden tager funktionen to argumenter som er heholdsvis `min` og `max` - altså de to tal man vil have et tal indenfor. Og i selve funktionen giver den ved hjælp af `.floor` et tal som bliver afrundet til den **nærmeste** `integer`-værdi (det vil sige et helt tal *uden* decimaler) - det tal den giver er bestemt af `.random` ganget med `max` (104, altså `max` - 1, som er `min` + 1 = 104) og senere tilføjes `min`, for at både `min`-værdien og `max`-værdien kan blive trukket som det tilfældige nummer.

Metoden jeg har brugt til at printe de fonetiske ord på kanvasset er ved hjælp af et `if`-statement, nu hvor jeg har delt ordene op i konsonanter og vokaler. Så først definerede variablen `let x` for at bestemme ordenes startende x-position på kanvasset og dernæst en variabel `let y` for at definere den startende y-position. Hvilket ord der skal printes, skulle dog bestemmes af de tilfældige numre, og for at overhovedet at finde et nummer kaldte jeg min funktion `randomIntFromInterval` som `rndInt` (for randomInteger) øverst i `draw()`. Denne brugte jeg derefter som argument i mit `if`-statement, så hvis `rndInt` var *mindre end* eller *lig med* 28 (som er syv vokaler ganget med fire - det er derfor at random generatoren er blevet sat til `max` 104) bliver en vokal printet. Dette passer med at hvis der bliver som er *større end* 28 skal en konsonant printes. For at printe ordene på kanvasset, blev jeg nødt til at lave en variabel `let phonicWord` som er et tilfældigt ord fra enten min vokal-array eller konsonant-array, der bliver fundet ved `random(phonicVowel)` eller `random(phonicConsonant)`. Denne lavede jeg herefter om et til en tekst-*string* ved at kalde `text(phonicWord)` på den startende `x`- og `y`-position. For så at få den rigtige afstand mellem ordene (så de ikke kom til at stå oveni hinanden) kunne jeg bruge p5.js-funktionen `textWidth` til at få den helt præcise afstand; det gjorde jeg ved at lave en ny variabel `let phonicWidth` og senere kalde `textWidth(phonicWord)`, for at tilføje afstanden til ordet tilføjede jeg `spacingX` (som er i de globale scope) til `phonicWidth`, og derefter tilføjede jeg de to til `x` hver gang at koden bliver eksekveret:
```
if (rndInt <= 28) {
  fill(random(50, 155));
  let phonicWord = random(phonicVowel)
  text(phonicWord, x, y);
  let phonicWidth = textWidth(phonicWord);
  x += phonicWidth + spacingX;
} else {
  fill(0, random(50,135), 0);
  let phonicWord = random(phonicConsonant)
  text(phonicWord, x, y);
  let phonicWidth = textWidth(phonicWord);
  x += phonicWidth + spacingX;
} 
```
Det sidste `if`-statement bliver brugt til at starte den næste række ord under den forrige række ved at sætte `x` til den originale startposition og tilføje `spacingY` (som i mit tilfælde er 25 pixels) for at få en afstand mellem ordene vertikalt:
```
if (x > width) {
  x = 125;
  y += spacingY;
}
```

### ReadMe
I min miniX har jeg taget inspiration fra historien (eller udviklingen) af software; software har i gennem mange år været under indflydelse fra militære organisationer, navngivenligt i USA, og militær- og våbenindustrien har derfor været en drivende faktor indenfor udviklingen af software. Hvis man skulle tage Alan Turing som et eksempel, udviklede han en udvidet, eller forbedret, version af enigma-maskinen fra Første Verdenskrig for at afkode tyske u-båds meddelser, der er bedre kendt som *the Bombe*, som den nu engang hedder. Der er andre eksmepler, hvor militær-industrien ikke bare har drevet software, men også større videnskabelige felter; her kunne et eksempel være *Manhattan Projektet*, der ikke bare fremme det molekylær-videnskabelige felt, men inddrog de fleste større videnskaber som kemi-, fysik- og ingeniørstudier. Kigger man i nyere og mere specifikt på det digitale, behøver man ikke at kigge længere end til cyberwarfare, der i stor stil benytter sig af algoritmer til at angribe store statslige systemer, og på den anden foreslår flere artikler at softwaresystemer er **kritiske** i forhold til at muliggøre brugen af moderne militære våbensystemer som f.eks. *Patriot* missilforsvarssystemet, F35-jagerfly og mange andre "fancy" opfindelser. 

***

I min miniX har jeg brugt regler som `if`- og `else if`-statements, som jeg har brugt til at genererer tekst på tværs og langs af kanvasset. Derudover har jeg også inkluderet en **auto-generator** i form af en `random` tal-"simulator", der giver et tal for hver gang der skal printes, eller skrives, et nyt ord på kanvaset. Reglerne producerer en slags "opførsel" i form af, at det er det tal, der bliver hentet fra tal-generatoren, som bestemmer om det er et langt, kort eller mellemlangt fonisk ord, der som sagt skal printes - dette gør, at man nok højstsandsynligt aldrig ville få det samme til at stå på skærmen (i hvertfald ikke i den samme rækkefølge). I forhold til reglerne i programmet, sker der det, at mit program over tid fylder siden op med de foniske ord - og hvis man snakker om regler, vil det jo egentlig aldrig blive 100% de samme ord på skærmen, men mere om det senere.

I forhold til idéen om en "auto-generator", så tænker jeg at min miniX spiller godt overens med mine generelle tanker om generators: En stykke kode med et ***komplet*** tilfældigt udfald, som jeg egentlig føler, ikke rigtig har noget personlighed over sig. Ser man på min egen miniX, så er det heller noget spændende som sådan over det auto-genererede - det er bare ord på en skærm, men jeg vil dog sige, at noget auto-genereret kunst godt kan være spændende og interessant, men generelt føler jeg at det er en smule... livløst, måske? Jeg tror, for mit vedkommende, at det handler om denne autonomi der er ved auto-genererede webpages, kunst eller lignende, at dét at der ikke er nogen form for interaktivitet gør, at man måske bare føler sig lidt som en tilskuer i noget der er bestemt på forhånd. Ja, man kan sige at der muligvis kan være noget "`random`" ved programmet eller kunsten, men for mig ændrer det stadig ikke på at størstedelen af hvad der skal ske er forudbestemt; det er bare det endelige resultat der egentlig er "`random`". Så hvis jeg skulle på et eller andet kunne det måske være, at denne her "*love*", der er ved for eksempel et maleri af Andy Warhol eller Picasso (nok en lidt mærkelig sammensætning, men jeg håber at pointen kommer i gennem), hvor man kan mærke tankerne og måske endda "følelserne" bagved, ikke kommer på spil i samme stil ved et auto-genereret stykke software art.

***
### Referenceliste

Software-defined Defence: Algorithms at War. (2023, Februar 17). IISS. https://www.iiss.org/blogs/research-paper/2023/02/software-defined-defence.
